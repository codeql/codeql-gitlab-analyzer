package main

import (
	"bufio"
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

var SupportLanguages = map[string]string{
	"cpp":        "cpp",
	"csharp":     "csharp",
	"go":         "go",
	"java":       "java",
	"javascript": "javascript",
	"typescript": "javascript",
	"python":     "python",
	"ruby":       "ruby",
}

const (
	FlagLanguage   = "language"
	FlagCommand    = "command"
	FlagSourceRoot = "source-root"
	FlagOutput     = "output"
	FlagCodeQLPath = "codeql-path"

	OutputFolder = ".codeql"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:    FlagCodeQLPath,
			Usage:   "Path to codeql. Default is codeql",
			Value:   "codeql",
			EnvVars: []string{"CODEQL_PATH"},
		},
		&cli.StringFlag{
			Name:    FlagLanguage,
			Aliases: []string{"l"},
			Usage:   "The language that the new database will be used to analyze",
			EnvVars: []string{"PROJECT_LANGUAGES", "CODEQL_LANGUAGES", "CI_PROJECT_REPOSITORY_LANGUAGES"},
		},
		&cli.StringFlag{
			Name:    FlagCommand,
			Aliases: []string{"c"},
			Usage:   "For compiled languages, build commands that will cause the compiler to be invoked on thesource code to analyze",
			EnvVars: []string{"BUILD_COMMAND"},
		},
		&cli.StringFlag{
			Name:    FlagSourceRoot,
			Aliases: []string{"s"},
			Usage:   "[Default: .] The root source code directory. In many cases, this will be the checkout root. Files within it are considered to be the primary source files for this database. In some output formats, files will be referred to by their relative path from this directory.",
			Value:   ".",
			EnvVars: []string{"SOURCE_DIR"},
		},
		&cli.StringFlag{
			Name:    FlagOutput,
			Aliases: []string{"o"},
			Value:   OutputFolder,
			Usage:   "The directory of create database codeql",
		},
	}
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	analyzer := Analyzer{
		CodeQLPath:   c.String(FlagCodeQLPath),
		Language:     c.String(FlagLanguage),
		SourceRoot:   c.String(FlagSourceRoot),
		OutputDir:    c.String(FlagOutput),
		BuildCommand: c.String(FlagCommand),
	}
	return analyzer.Analyze()
}

type Analyzer struct {
	CodeQLPath   string
	Language     string
	SourceRoot   string
	OutputDir    string
	BuildCommand string
	_languages   []string
}

func (this *Analyzer) Analyze() (io.ReadCloser, error) {
	outputDir := this.OutputDir
	if _, err := os.Stat(outputDir); os.IsNotExist(err) {
		_ = os.Mkdir(outputDir, os.ModePerm)
	}
	_ = this.BuildDatabase()
	var err error
	defer func() {
		if err != nil {
			log.Error("Analyze error: " + err.Error())
			os.Exit(2)
		} else {
			log.Info("Analyze success")
		}
	}()
	var results []string
	for _, language := range this.Languages() {
		db := filepath.Join(outputDir, language+"-database")
		resultOutput := filepath.Join(outputDir, fmt.Sprintf("%s_codeql.sarif", language))
		// threads = 0 matches the number of threads to the number of logical processors
		queries := this.QlPacksByLanguage(language)
		args := []string{
			"database", "analyze", "--sarif-add-query-help",
			"--format", "sarif-latest", "--threads", "0", "--output", resultOutput, db,
		}
		args = append(args, queries...)
		cmd := exec.Command(this.CodeQLPath, args...)
		cmd.Env = os.Environ()
		log.Info(fmt.Sprintf("Analyze %s with queries: %s", language, strings.Join(queries, " ")))
		log.Info(cmd.String())
		stdout, _ := cmd.StdoutPipe()
		err = cmd.Start()
		if err != nil {
			return nil, err
		}
		go printStdout(stdout)
		err = cmd.Wait()
		if err == nil {
			results = append(results, resultOutput)
			log.Info("Analyze " + language + " success")
		} else {
			return nil, err
		}
	}
	resultsFile := filepath.Join(outputDir, "results.txt")
	_ = ioutil.WriteFile(resultsFile, []byte(strings.Join(results, "\n")), 0777)
	return os.Open(resultsFile)
}

func (this *Analyzer) BuildDatabase() error {
	var err error
	defer func() {
		if err != nil {
			log.Error("Build database error: " + err.Error())
			os.Exit(2)
		} else {
			log.Info("Build database success")
		}
	}()
	languages := this.Languages()
	if len(languages) == 0 {
		err = errors.New("languages required")
		return err
	}
	for _, language := range languages {
		options := []string{"database", "create", "--language", language, "--source-root",
			this.SourceRoot, "--overwrite", filepath.Join(this.OutputDir, language+"-database")}
		if this.BuildCommand != "" {
			options = append(options, []string{"--command", this.BuildCommand}...)
		}
		cmd := exec.Command(this.CodeQLPath, options...)
		log.Info(fmt.Sprintf("Build %s database", language))
		log.Info(cmd.String())
		stdout, _ := cmd.StdoutPipe()
		err = cmd.Start()
		if err != nil {
			return err
		}
		go printStdout(stdout)
		err = cmd.Wait()
	}
	return err
}

func (this *Analyzer) Languages() []string {
	if len(this._languages) == 0 {
		for _, lang := range strings.Split(strings.TrimSpace(this.Language), ",") {
			if SupportLanguages[lang] != "" {
				this._languages = append(this._languages, SupportLanguages[lang])
			} else {
				log.Warn("CodeQL don't support language " + lang)
			}
		}
	}
	return this._languages
}

func (this *Analyzer) QlPacksByLanguage(language string) []string {
	var results []string
	cmd := exec.Command(this.CodeQLPath, "resolve", "qlpacks", "--kind", "query")
	cmd.Env = os.Environ()
	data, err := cmd.Output()
	if err == nil {
		lines := strings.Split(string(data), "\n")
		for _, line := range lines {
			query := strings.Split(line, " ")[0]
			query = strings.TrimSpace(query)
			if strings.Contains(query, language+"-queries") {
				results = append(results, query)
			}
		}
	}
	return results
}

func printStdout(stdout io.ReadCloser) {
	reader := bufio.NewReader(stdout)
	line, _, err := reader.ReadLine()
	for {
		if err != nil || line == nil {
			break
		}
		log.Println(string(line))
		line, _, err = reader.ReadLine()
	}
}
