package metadata

import (
	"fmt"
	"os"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v3"
)

const (
	// AnalyzerVendor is the vendor/maintainer of the analyzer
	AnalyzerVendor = "CodeQL"

	// AnalyzerID identifies the analyzer
	AnalyzerID = "codeql"

	// AnalyzerName is the name of the analyzer
	AnalyzerName = "CodeQL"

	AnalyzerURL = "https://codeql.github.com"
	// Type returns the type of the scan
	Type report.Category = report.CategorySast
)

var (
	// AnalyzerVersion is a placeholder value which the Dockerfile will dynamically
	// overwrite at build time with the most recent version from the CHANGELOG.md file
	AnalyzerVersion = "not-configured"

	// ScannerVersion is the semantic version of the scanner and is defined in the Dockerfile
	ScannerVersion = os.Getenv("SCANNER_VERSION")
	// VulnerabilityScanner describes the scanner used to find a vulnerability
	VulnerabilityScanner = report.Scanner{
		ID:   AnalyzerID,
		Name: AnalyzerName,
	}

	// ReportScanner returns identifying information about a security scanner
	ReportScanner = report.ScannerDetails{
		ID:      AnalyzerID,
		Name:    AnalyzerName,
		Version: ScannerVersion,
		Vendor: report.Vendor{
			Name: AnalyzerVendor,
		},
		URL: AnalyzerURL,
	}

	// AnalyzerUsage provides a one line usage string for the analyzer
	AnalyzerUsage = fmt.Sprintf("%s %s analyzer v%s", AnalyzerVendor, AnalyzerName, AnalyzerVersion)
)
