module gitlab.com/codeql/codeql-gitlab-analyzer

require (
	github.com/google/go-cmp v0.5.7
	github.com/sirupsen/logrus v1.8.1
	github.com/urfave/cli/v2 v2.3.0
	gitlab.com/gitlab-org/security-products/analyzers/command v1.6.1
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.24.1
	gitlab.com/gitlab-org/security-products/analyzers/report/v3 v3.10.0
	gitlab.com/gitlab-org/security-products/analyzers/ruleset v1.4.0
)

go 1.15
