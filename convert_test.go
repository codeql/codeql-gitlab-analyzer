package main

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v3"
)

func TestConvert(t *testing.T) {
	in := "test/codeql.sarif"
	os.Stat(in)
	if _, err := os.Stat(in); err != nil {
		t.Fatal("test/codeql.sarif not found")
	}
	r := strings.NewReader(in)
	content, err := ioutil.ReadFile("test/sast_expert.json")
	if err != nil {
		t.Fatal("test/sast_expert.json not found")
	}
	var want report.Report
	err = json.Unmarshal(content, &want)
	if err != nil {
		t.Fatal(err)
	}
	got, err := convert(r, "app")
	if err != nil {
		t.Fatal(err)
	}
	if diff := cmp.Diff(want, got); diff != "" {
		t.Errorf("Report mismatch (-want +got):\n%s", diff)
	}
}
