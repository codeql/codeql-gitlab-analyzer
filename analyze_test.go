package main

import (
	"fmt"
	"strings"
	"testing"
)

func TestQlPackByLanguageFuntion(t *testing.T) {
	analyzer := Analyzer{CodeQLPath: "/opt/codeql/codeql"}
	queries := analyzer.QlPacksByLanguage("java")
	fmt.Println(strings.Join(queries, " "))
}

func TestAnalyzer(t *testing.T) {
	analyzer := Analyzer{
		CodeQLPath:   "/opt/codeql/codeql",
		Language:     "java",
		SourceRoot:   "~/Projects/vulnado/",
		OutputDir:    "~/Projects/vulnado/.codeql",
		BuildCommand: "mvn clean install",
	}
	_, err := analyzer.Analyze()
	if err != nil {
		return
	}
}
